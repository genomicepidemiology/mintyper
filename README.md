Please use the link below to install and run an updated version of MINTyper:
> https://github.com/MBHallgren/MINTyper

Read more about MINTyper here:
> https://academic.oup.com/biomethods/article/6/1/bpab008/6243723